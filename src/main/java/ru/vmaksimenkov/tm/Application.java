package ru.vmaksimenkov.tm;

import ru.vmaksimenkov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        if (run(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command: ");
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    private static void displayHelp() {
        System.out.println(TerminalConst.CMD_VERSION + " - Display program version.");
        System.out.println(TerminalConst.CMD_ABOUT + " - Display developer info.");
        System.out.println(TerminalConst.CMD_HELP + " - Display list of terminal commands.");
        System.out.println(TerminalConst.CMD_EXIT + " - Close applicationhel.");
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("Vlad Maximenkov");
        System.out.println("vmaksimenkov@tsconsulting.com");
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void incorrectCommand() {
        System.out.println("Unknown command. Type help to see all available commands");
    }

    private static boolean run(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void sysExit() {
        System.exit(0);
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case TerminalConst.CMD_HELP: displayHelp(); break;
            case TerminalConst.CMD_VERSION: displayVersion(); break;
            case TerminalConst.CMD_ABOUT: displayAbout(); break;
            case TerminalConst.CMD_EXIT: sysExit(); break;
            default: incorrectCommand();
        }
    }
    
}
